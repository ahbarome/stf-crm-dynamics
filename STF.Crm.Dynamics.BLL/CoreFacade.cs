﻿using STF.Crm.Dynamics.BLL.Operation;
using STF.Crm.Dynamics.DTO.Quality;

namespace STF.Crm.Dynamics.BLL
{
    public class CoreFacade
    {
        #region Attributes

        private static ContactHandler _contact;

        #endregion

        #region Properties

        private static ContactHandler Contact
        {
            get
            {
                return _contact ?? new ContactHandler();
            }
        }

        #endregion

        #region Methods

        public static CustomerResponse GetCustomerById(string customerId)
        {
            return Contact.GetCustomerById(customerId);
        }

        #endregion
    }
}

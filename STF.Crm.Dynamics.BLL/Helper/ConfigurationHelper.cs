﻿using System.Configuration;

namespace STF.Crm.Dynamics.BLL.Helper
{
    internal class ConfigurationHelper
    {
        internal static string DiscoveryServiceAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["DiscoveryServiceAddress"];
            }
        }

        internal static string OrganizationUniqueName
        {
            get
            {
                return ConfigurationManager.AppSettings["OrganizationUniqueName"];
            }
        }

        internal static string CrmUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["CrmUserName"];
            }
        }

        internal static string CrmPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["CrmPassword"];
            }
        }

        internal static string Domain
        {
            get
            {
                return ConfigurationManager.AppSettings["Domain"];
            }
        }
    }
}

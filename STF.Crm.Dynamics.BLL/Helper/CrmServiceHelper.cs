﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel.Description;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace STF.Crm.Dynamics.BLL.Helper
{
    public static class DeviceIdManager
    {
        #region Fields

        private static readonly Random RandomInstance = new Random();

        public const int MaxDeviceNameLength = 24;
        public const int MaxDevicePasswordLength = 24;

        #endregion

        #region Constructor

        static DeviceIdManager()
        {
            PersistToFile = true;
        }

        #endregion

        #region Properties

        public static bool PersistToFile { get; set; }

        public static bool PersistIfDeviceAlreadyExists { get; set; }

        #endregion

        #region Methods


        public static ClientCredentials LoadOrRegisterDevice()
        {
            return LoadOrRegisterDevice(null);
        }

        public static ClientCredentials LoadOrRegisterDevice(string deviceName, string devicePassword)
        {
            return LoadOrRegisterDevice(null, deviceName, devicePassword);
        }

        public static ClientCredentials LoadOrRegisterDevice(Uri issuerUri)
        {
            return LoadOrRegisterDevice(issuerUri, null, null);
        }

        public static ClientCredentials LoadOrRegisterDevice(Uri issuerUri, string deviceName, string devicePassword)
        {
            var credentials = LoadDeviceCredentials(issuerUri);

            if (null == credentials)
            {
                credentials = RegisterDevice(Guid.NewGuid(), issuerUri, deviceName, devicePassword);
            }

            return credentials;
        }

        public static ClientCredentials RegisterDevice()
        {
            return RegisterDevice(Guid.NewGuid());
        }

        public static ClientCredentials RegisterDevice(Guid applicationId)
        {
            return RegisterDevice(applicationId, (Uri)null);
        }

        public static ClientCredentials RegisterDevice(Guid applicationId, Uri issuerUri)
        {
            return RegisterDevice(applicationId, issuerUri, null, null);
        }

        public static ClientCredentials RegisterDevice(Guid applicationId, string deviceName, string devicePassword)
        {
            return RegisterDevice(applicationId, (Uri)null, deviceName, devicePassword);
        }

        public static ClientCredentials RegisterDevice(Guid applicationId, Uri issuerUri, string deviceName, string devicePassword)
        {
            if (string.IsNullOrEmpty(deviceName) && !PersistToFile)
            {
                throw new ArgumentNullException("deviceName", "If PersistToFile is false, then deviceName must be specified.");
            }
            else if (string.IsNullOrEmpty(deviceName) != string.IsNullOrEmpty(devicePassword))
            {
                throw new ArgumentNullException("deviceName", "Either deviceName/devicePassword should both be specified or they should be null.");
            }

            var device = GenerateDevice(deviceName, devicePassword);

            return RegisterDevice(applicationId, issuerUri, device);
        }

        public static ClientCredentials LoadDeviceCredentials()
        {
            return LoadDeviceCredentials(null);
        }

        public static ClientCredentials LoadDeviceCredentials(Uri issuerUri)
        {
            if (!PersistToFile)
            {
                return null;
            }

            var environment = DiscoverEnvironmentInternal(issuerUri);
            var device = ReadExistingDevice(environment);

            if (null == device || null == device.User)
            {
                return null;
            }

            return device.User.ToClientCredentials();
        }

        public static string DiscoverEnvironment(Uri issuerUri)
        {
            return DiscoverEnvironmentInternal(issuerUri).Environment;
        }

        #endregion

        #region Private Methods

        private static EnvironmentConfiguration DiscoverEnvironmentInternal(Uri issuerUri)
        {
            if (null == issuerUri)
            {
                return new EnvironmentConfiguration(EnvironmentType.LiveDeviceID, "login.live.com", null);
            }

            var searchList = new Dictionary<EnvironmentType, string>();
            searchList.Add(EnvironmentType.LiveDeviceID, "login.live");
            searchList.Add(EnvironmentType.OrgDeviceID, "login.microsoftonline");

            foreach (KeyValuePair<EnvironmentType, string> searchPair in searchList)
            {
                if (issuerUri.Host.Length > searchPair.Value.Length && issuerUri.Host.StartsWith(searchPair.Value, StringComparison.OrdinalIgnoreCase))
                {
                    var environment = issuerUri.Host.Substring(searchPair.Value.Length);

                    if ('-' == environment[0])
                    {
                        var separatorIndex = environment.IndexOf('.', 1);

                        if (-1 != separatorIndex)
                        {
                            environment = environment.Substring(1, separatorIndex - 1);
                        }
                        else
                        {
                            environment = null;
                        }
                    }
                    else
                    {
                        environment = null;
                    }

                    return new EnvironmentConfiguration(searchPair.Key, issuerUri.Host, environment);
                }
            }

            return new EnvironmentConfiguration(EnvironmentType.LiveDeviceID, issuerUri.Host, null);
        }

        private static void Serialize<T>(Stream stream, T value)
        {
            var serializer = new XmlSerializer(typeof(T), string.Empty);

            var xmlNamespaces = new XmlSerializerNamespaces();
            xmlNamespaces.Add(string.Empty, string.Empty);

            serializer.Serialize(stream, value, xmlNamespaces);
        }

        private static T Deserialize<T>(string operationName, Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                return Deserialize<T>(operationName, reader.ReadToEnd());
            }
        }

        private static T Deserialize<T>(string operationName, string xml)
        {
            using (var reader = new StringReader(xml))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T), string.Empty);
                    return (T)serializer.Deserialize(reader);
                }
                catch (InvalidOperationException ex)
                {
                    throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Unable to Deserialize XML (Operation = {0}):{1}{2}", operationName, Environment.NewLine, xml), ex);
                }
            }
        }

        private static FileInfo GetDeviceFile(EnvironmentConfiguration environment)
        {
            return new FileInfo(string.Format(CultureInfo.InvariantCulture, LiveIdConstants.FileNameFormat, environment.Type, string.IsNullOrEmpty(environment.Environment) ? null : "-" + environment.Environment.ToUpperInvariant()));
        }

        private static ClientCredentials RegisterDevice(Guid applicationId, Uri issuerUri, LiveDevice device)
        {
            var environment = DiscoverEnvironmentInternal(issuerUri);

            var request = new DeviceRegistrationRequest(applicationId, device);

            var url = string.Format(CultureInfo.InvariantCulture, LiveIdConstants.RegistrationEndpointUriFormat, environment.HostName);

            var response = ExecuteRegistrationRequest(url, request);

            if (!response.IsSuccess)
            {
                var throwException = true;

                if (DeviceRegistrationErrorCode.DeviceAlreadyExists == response.Error.RegistrationErrorCode)
                {
                    if (!PersistToFile)
                    {
                        return device.User.ToClientCredentials();
                    }
                    else if (PersistIfDeviceAlreadyExists)
                    {
                        throwException = false;
                    }
                }

                if (throwException)
                {
                    throw new DeviceRegistrationFailedException(response.Error.RegistrationErrorCode, response.ErrorSubCode);
                }
            }

            if (PersistToFile || PersistIfDeviceAlreadyExists)
            {
                WriteDevice(environment, device);
            }

            return device.User.ToClientCredentials();
        }

        private static LiveDevice GenerateDevice(string deviceName, string devicePassword)
        {
            DeviceUserName userNameCredentials;

            if (string.IsNullOrEmpty(deviceName))
            {
                userNameCredentials = GenerateDeviceUserName();
            }
            else
            {
                userNameCredentials = new DeviceUserName() { DeviceName = deviceName, DecryptedPassword = devicePassword };
            }

            return new LiveDevice() { User = userNameCredentials, Version = 1 };
        }

        private static LiveDevice ReadExistingDevice(EnvironmentConfiguration environment)
        {
            var file = GetDeviceFile(environment);

            if (!file.Exists)
            {
                return null;
            }

            using (var stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return Deserialize<LiveDevice>("Loading Device Credentials from Disk", stream);
            }
        }

        private static void WriteDevice(EnvironmentConfiguration environment, LiveDevice device)
        {
            var file = GetDeviceFile(environment);

            if (!file.Directory.Exists)
            {
                file.Directory.Create();
            }

            using (var stream = file.Open(FileMode.CreateNew, FileAccess.Write, FileShare.None))
            {
                Serialize(stream, device);
            }
        }

        private static DeviceRegistrationResponse ExecuteRegistrationRequest(string url, DeviceRegistrationRequest registrationRequest)
        {
            var request = WebRequest.Create(url);
            request.ContentType = "application/soap+xml; charset=UTF-8";
            request.Method = "POST";
            request.Timeout = 180000;

            using (var stream = request.GetRequestStream())
            {
                Serialize(stream, registrationRequest);
            }

            try
            {
                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        return Deserialize<DeviceRegistrationResponse>("Deserializing Registration Response", stream);
                    }
                }
            }
            catch (WebException ex)
            {
                System.Diagnostics.Trace.TraceError("Microsoft account Device Registration Failed (HTTP Code: {0}): {1}", ex.Status, ex.Message);

                if (null != ex.Response)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    {
                        return Deserialize<DeviceRegistrationResponse>("Deserializing Failed Registration Response", stream);
                    }
                }

                throw;
            }
        }

        private static DeviceUserName GenerateDeviceUserName()
        {
            var userName = new DeviceUserName();
            userName.DeviceName = GenerateRandomString(LiveIdConstants.ValidDeviceNameCharacters, MaxDeviceNameLength);
            userName.DecryptedPassword = GenerateRandomString(LiveIdConstants.ValidDevicePasswordCharacters, MaxDevicePasswordLength);

            return userName;
        }

        private static string GenerateRandomString(string characterSet, int count)
        {
            char[] value = new char[count];
            char[] set = characterSet.ToCharArray();

            lock (RandomInstance)
            {
                for (var i = 0; i < count; i++)
                {
                    value[i] = set[RandomInstance.Next(0, set.Length)];
                }
            }

            return new string(value);
        }

        #endregion

        #region Private Classes

        private enum EnvironmentType
        {
            LiveDeviceID,
            OrgDeviceID
        }

        private sealed class EnvironmentConfiguration
        {
            public EnvironmentConfiguration(EnvironmentType type, string hostName, string environment)
            {
                if (string.IsNullOrWhiteSpace(hostName))
                {
                    throw new ArgumentNullException("hostName");
                }

                this.Type = type;
                this.HostName = hostName;
                this.Environment = environment;
            }

            #region Properties

            public EnvironmentType Type { get; private set; }

            public string HostName { get; private set; }

            public string Environment { get; private set; }

            #endregion
        }

        private static class LiveIdConstants
        {
            public const string RegistrationEndpointUriFormat = @"https://{0}/ppsecure/DeviceAddCredential.srf";

            public static readonly string FileNameFormat = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "LiveDeviceID"), "{0}{1}.xml");

            public const string ValidDeviceNameCharacters = "0123456789abcdefghijklmnopqrstuvqxyz";

            public const string ValidDevicePasswordCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^*()-_=+;,./?`~";
        }

        #endregion
    }

    #region Public Classes & Enums

    public enum DeviceRegistrationErrorCode
    {
        Unknown = 0,
        InterfaceDisabled = 1,
        InvalidRequestFormat = 3,
        UnknownClientVersion = 4,
        BlankPassword = 6,
        MissingDeviceUserNameOrPassword = 7,
        InvalidParameterSyntax = 8,
        InvalidCharactersInCredentials = 9,
        InternalError = 11,
        DeviceAlreadyExists = 13
    }

    [Serializable]
    public sealed class DeviceRegistrationFailedException : Exception
    {
        public DeviceRegistrationFailedException()
            : base()
        {
        }

        public DeviceRegistrationFailedException(string message)
            : base(message)
        {
        }

        public DeviceRegistrationFailedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public DeviceRegistrationFailedException(DeviceRegistrationErrorCode code, string subCode)
            : this(code, subCode, null)
        {
        }

        public DeviceRegistrationFailedException(DeviceRegistrationErrorCode code, string subCode, Exception innerException)
            : base(string.Concat(code.ToString(), ": ", subCode), innerException)
        {
            this.RegistrationErrorCode = code;
        }

        private DeviceRegistrationFailedException(SerializationInfo si, StreamingContext sc)
            : base(si, sc)
        {
        }

        #region Properties

        public DeviceRegistrationErrorCode RegistrationErrorCode { get; private set; }

        #endregion

        #region Methods

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }

    #region Serialization Classes

    #region DeviceRegistrationRequest Class

    [EditorBrowsable(EditorBrowsableState.Never)]
    [XmlRoot("DeviceAddRequest")]
    public sealed class DeviceRegistrationRequest
    {
        #region Constructors

        public DeviceRegistrationRequest()
        {
        }

        public DeviceRegistrationRequest(Guid applicationId, LiveDevice device)
            : this()
        {
            if (null == device)
            {
                throw new ArgumentNullException("device");
            }

            this.ClientInfo = new DeviceRegistrationClientInfo() { ApplicationId = applicationId, Version = "1.0" };
            this.Authentication = new DeviceRegistrationAuthentication()
            {
                MemberName = device.User.DeviceId,
                Password = device.User.DecryptedPassword
            };
        }

        #endregion

        #region Properties

        [XmlElement("ClientInfo")]
        public DeviceRegistrationClientInfo ClientInfo { get; set; }

        [XmlElement("Authentication")]
        public DeviceRegistrationAuthentication Authentication { get; set; }

        #endregion
    }

    #endregion

    #region DeviceRegistrationClientInfo Class

    [EditorBrowsable(EditorBrowsableState.Never)]
    [XmlRoot("ClientInfo")]
    public sealed class DeviceRegistrationClientInfo
    {
        #region Properties

        [XmlAttribute("name")]
        public Guid ApplicationId { get; set; }

        [XmlAttribute("version")]
        public string Version { get; set; }

        #endregion
    }

    #endregion

    #region DeviceRegistrationAuthentication Class

    [EditorBrowsable(EditorBrowsableState.Never)]
    [XmlRoot("Authentication")]
    public sealed class DeviceRegistrationAuthentication
    {
        #region Properties

        [XmlElement("Membername")]
        public string MemberName { get; set; }

        [XmlElement("Password")]
        public string Password { get; set; }

        #endregion
    }

    #endregion

    #region DeviceRegistrationResponse Class

    [EditorBrowsable(EditorBrowsableState.Never)]
    [XmlRoot("DeviceAddResponse")]
    public sealed class DeviceRegistrationResponse
    {
        #region Properties

        [XmlElement("success")]
        public bool IsSuccess { get; set; }

        [XmlElement("puid")]
        public string Puid { get; set; }

        [XmlElement("Error")]
        public DeviceRegistrationResponseError Error { get; set; }

        [XmlElement("ErrorSubcode")]
        public string ErrorSubCode { get; set; }

        #endregion
    }

    #endregion

    #region DeviceRegistrationResponse Class

    [EditorBrowsable(EditorBrowsableState.Never)]
    [XmlRoot("Error")]
    public sealed class DeviceRegistrationResponseError
    {
        private string _code;

        #region Properties

        [XmlAttribute("Code")]
        public string Code
        {
            get
            {
                return this._code;
            }

            set
            {
                this._code = value;

                if (!string.IsNullOrEmpty(value))
                {
                    if (value.StartsWith("dc", StringComparison.Ordinal))
                    {
                        int code;
                        if (int.TryParse(value.Substring(2), NumberStyles.Integer, CultureInfo.InvariantCulture, out code) && Enum.IsDefined(typeof(DeviceRegistrationErrorCode), code))
                        {
                            this.RegistrationErrorCode = (DeviceRegistrationErrorCode)Enum.ToObject(typeof(DeviceRegistrationErrorCode), code);
                        }
                    }
                }
            }
        }

        [XmlIgnore]
        public DeviceRegistrationErrorCode RegistrationErrorCode { get; private set; }

        #endregion
    }

    #endregion

    #region LiveDevice Class

    [EditorBrowsable(EditorBrowsableState.Never)]
    [XmlRoot("Data")]
    public sealed class LiveDevice
    {
        #region Properties

        [XmlAttribute("version")]
        public int Version { get; set; }

        [XmlElement("User")]
        public DeviceUserName User { get; set; }

        [SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId = "System.Xml.XmlNode", Justification = "This is required for proper XML Serialization")]
        [XmlElement("Token")]
        public XmlNode Token { get; set; }

        [XmlElement("Expiry")]
        public string Expiry { get; set; }

        [XmlElement("ClockSkew")]
        public string ClockSkew { get; set; }

        #endregion
    }

    #endregion

    #region DeviceUserName Class

    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class DeviceUserName
    {
        private string _encryptedPassword;
        private string _decryptedPassword;
        private bool _encryptedValueIsUpdated;

        #region Constants

        private const string UserNamePrefix = "11";

        #endregion

        #region Constructors

        public DeviceUserName()
        {
            this.UserNameType = "Logical";
        }

        #endregion

        #region Properties

        [XmlAttribute("username")]
        public string DeviceName { get; set; }

        [XmlAttribute("type")]
        public string UserNameType { get; set; }

        [XmlElement("Pwd")]
        public string EncryptedPassword
        {
            get
            {
                this.ThrowIfNoEncryption();

                if (!this._encryptedValueIsUpdated)
                {
                    this._encryptedPassword = this.Encrypt(this._decryptedPassword);
                    this._encryptedValueIsUpdated = true;
                }

                return this._encryptedPassword;
            }

            set
            {
                this.ThrowIfNoEncryption();
                this.UpdateCredentials(value, null);
            }
        }

        public string DeviceId
        {
            get
            {
                return UserNamePrefix + DeviceName;
            }
        }

        [XmlIgnore]
        public string DecryptedPassword
        {
            get
            {
                return this._decryptedPassword;
            }

            set
            {
                this.UpdateCredentials(null, value);
            }
        }

        private bool IsEncryptionEnabled
        {
            get
            {
                return DeviceIdManager.PersistToFile;
            }
        }

        #endregion

        #region Methods

        public ClientCredentials ToClientCredentials()
        {
            var credentials = new ClientCredentials();
            credentials.UserName.UserName = this.DeviceId;
            credentials.UserName.Password = this.DecryptedPassword;

            return credentials;
        }

        private void ThrowIfNoEncryption()
        {
            if (!this.IsEncryptionEnabled)
            {
                throw new NotSupportedException("Not supported when DeviceIdManager.UseEncryptionApis is false.");
            }
        }

        private void UpdateCredentials(string encryptedValue, string decryptedValue)
        {
            var isValueUpdated = false;

            if (string.IsNullOrEmpty(encryptedValue) && string.IsNullOrEmpty(decryptedValue))
            {
                isValueUpdated = true;
            }
            else if (string.IsNullOrEmpty(encryptedValue))
            {
                if (this.IsEncryptionEnabled)
                {
                    encryptedValue = this.Encrypt(decryptedValue);
                    isValueUpdated = true;
                }
                else
                {
                    encryptedValue = null;
                    isValueUpdated = false;
                }
            }
            else
            {
                this.ThrowIfNoEncryption();

                decryptedValue = this.Decrypt(encryptedValue);
                isValueUpdated = true;
            }

            this._encryptedPassword = encryptedValue;
            this._decryptedPassword = decryptedValue;
            this._encryptedValueIsUpdated = isValueUpdated;
        }

        private string Encrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            var encryptedBytes = ProtectedData.Protect(Encoding.UTF8.GetBytes(value), null, DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedBytes);
        }

        private string Decrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            var decryptedBytes = ProtectedData.Unprotect(Convert.FromBase64String(value), null, DataProtectionScope.CurrentUser);
            if (null == decryptedBytes || 0 == decryptedBytes.Length)
            {
                return null;
            }

            return Encoding.UTF8.GetString(decryptedBytes, 0, decryptedBytes.Length);
        }

        #endregion
    }

    #endregion

    #endregion

    #endregion
}

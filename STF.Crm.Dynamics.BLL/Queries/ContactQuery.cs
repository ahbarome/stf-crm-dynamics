﻿using Microsoft.Xrm.Sdk.Query;

namespace STF.Crm.Dynamics.BLL.Queries
{
    internal class ContactQuery
    {
        #region Attributes

        private static string[] _columns = new string[] { "contactid", "ce_cardnumber", "ce_codecustomer", "ce_codecustomererp", "firstname", "fullname", "birthdate" };

        #endregion

        #region Methods

        internal static QueryExpression GetContactByFilter(string field, string value)
        {
            var query = new QueryExpression()
            {
                EntityName = Contact.EntityLogicalName,
                ColumnSet = new ColumnSet(_columns),
                Criteria =
                {
                    Conditions = 
                    {
                        new ConditionExpression (field, ConditionOperator.Like, value)
                    }
                }
            };

            return query;
        }

        #endregion
    }
}

﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using STF.Crm.Dynamics.BLL.Helper;
using System;
using System.ServiceModel.Description;

namespace STF.Crm.Dynamics.BLL.Connector
{
    public class CrmConnectionManager
    {
        #region Methods

        internal OrganizationServiceProxy RetrieveOrganizationProxy()
        {
            var serviceManagement = ServiceConfigurationFactory.CreateManagement<IDiscoveryService>(new Uri(ConfigurationHelper.DiscoveryServiceAddress));
            var endpointType = serviceManagement.AuthenticationType;

            var authCredentials = GetCredentials(serviceManagement, endpointType);
            var organizationUri = string.Empty;

            using (var discoveryProxy = GetProxy<IDiscoveryService, DiscoveryServiceProxy>(serviceManagement, authCredentials))
            {
                if (discoveryProxy != null)
                {
                    var orgs = DiscoverOrganizations(discoveryProxy);
                    organizationUri = FindOrganization(ConfigurationHelper.OrganizationUniqueName, orgs.ToArray()).Endpoints[EndpointType.OrganizationService];
                }
            }

            OrganizationServiceProxy organizationProxy = null;

            if (string.IsNullOrEmpty(organizationUri))
            {
                return organizationProxy;
            }

            var orgServiceManagement = ServiceConfigurationFactory.CreateManagement<IOrganizationService>(new Uri(organizationUri));
            var credentials = GetCredentials(orgServiceManagement, endpointType);

            organizationProxy = GetProxy<IOrganizationService, OrganizationServiceProxy>(orgServiceManagement, credentials);
            organizationProxy.EnableProxyTypes();

            return organizationProxy;
        }

        private AuthenticationCredentials GetCredentials<TService>(IServiceManagement<TService> service, AuthenticationProviderType endpointType)
        {
            var authCredentials = new AuthenticationCredentials();

            switch (endpointType)
            {
                case AuthenticationProviderType.ActiveDirectory:
                    authCredentials.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ConfigurationHelper.CrmUserName, ConfigurationHelper.CrmPassword, ConfigurationHelper.Domain);
                    break;
                case AuthenticationProviderType.LiveId:
                    authCredentials.ClientCredentials.UserName.UserName = ConfigurationHelper.CrmUserName;
                    authCredentials.ClientCredentials.UserName.Password = ConfigurationHelper.CrmPassword;
                    authCredentials.SupportingCredentials = new AuthenticationCredentials();
                    authCredentials.SupportingCredentials.ClientCredentials = DeviceIdManager.LoadOrRegisterDevice();
                    break;
                default:
                    authCredentials.ClientCredentials.UserName.UserName = ConfigurationHelper.CrmUserName;
                    authCredentials.ClientCredentials.UserName.Password = ConfigurationHelper.CrmPassword;

                    if (endpointType == AuthenticationProviderType.OnlineFederation)
                    {
                        var provider = service.GetIdentityProvider(authCredentials.ClientCredentials.UserName.UserName);
                        if (provider != null && provider.IdentityProviderType == IdentityProviderType.LiveId)
                        {
                            authCredentials.SupportingCredentials = new AuthenticationCredentials();
                            authCredentials.SupportingCredentials.ClientCredentials = DeviceIdManager.LoadOrRegisterDevice();
                        }
                    }

                    break;
            }

            return authCredentials;
        }

        private OrganizationDetailCollection DiscoverOrganizations(IDiscoveryService service)
        {
            if (service == null) throw new ArgumentNullException("service");

            var orgRequest = new RetrieveOrganizationsRequest();
            var orgResponse = (RetrieveOrganizationsResponse)service.Execute(orgRequest);

            return orgResponse.Details;
        }

        private OrganizationDetail FindOrganization(string orgUniqueName, OrganizationDetail[] orgDetails)
        {
            if (string.IsNullOrWhiteSpace(orgUniqueName))
            {
                throw new ArgumentNullException("orgUniqueName");
            }

            if (orgDetails == null)
            {
                throw new ArgumentNullException("orgDetails");
            }

            OrganizationDetail orgDetail = null;

            foreach (var detail in orgDetails)
            {
                if (string.Compare(detail.UniqueName, orgUniqueName, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    orgDetail = detail;
                    break;
                }
            }

            return orgDetail;
        }

        private TProxy GetProxy<TService, TProxy>(IServiceManagement<TService> serviceManagement, AuthenticationCredentials authCredentials)
            where TService : class
            where TProxy : ServiceProxy<TService>
        {
            var classType = typeof(TProxy);

            if (serviceManagement.AuthenticationType != AuthenticationProviderType.ActiveDirectory)
            {
                var tokenCredentials = serviceManagement.Authenticate(authCredentials);

                return (TProxy)classType.GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(SecurityTokenResponse) }).Invoke(new object[] { serviceManagement, tokenCredentials.SecurityTokenResponse });
            }

            return (TProxy)classType.GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(ClientCredentials) }).Invoke(new object[] { serviceManagement, authCredentials.ClientCredentials });
        }

        #endregion
    }
}

﻿using Microsoft.Crm.Sdk.Messages;
using STF.Crm.Dynamics.BLL.Connector;
using STF.Crm.Dynamics.BLL.Queries;
using STF.Crm.Dynamics.DTO.Quality;

namespace STF.Crm.Dynamics.BLL.Operation
{
    internal class ContactHandler
    {
        #region Methods

        internal CustomerResponse GetCustomerById(string customerId)
        {
            var connectionManager = new CrmConnectionManager();

            Contact contact = null;

            using (var organizationProxy = connectionManager.RetrieveOrganizationProxy())
            {
                var userid = ((WhoAmIResponse)organizationProxy.Execute(new WhoAmIRequest())).UserId;
                var contacts = organizationProxy.RetrieveMultiple(ContactQuery.GetContactByFilter("firstname", "diana")).Entities;

                contact = contacts[0] as Contact;
            }

            return new CustomerResponse
            {
                CustomerId = contact.ce_CardNumber,
                FullName = contact.FullName
            };
        }

        #endregion
    }
}

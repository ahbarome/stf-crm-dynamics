﻿using STF.Crm.Dynamics.BLL;
using System;

namespace STF.Crm.Dynamics.WS
{
    public class CrmDynamicsService : ICrmDynamicsService
    {
        #region ICrmDynamicsService

        public DTO.Quality.CustomerResponse GetCustomer(string customerId)
        {
            return CoreFacade.GetCustomerById(customerId);
        }

        public void SendCustomer(DTO.Quality.CustomerResponse customer)
        {
            throw new NotImplementedException();
        }

        public void SendIncident(DTO.Quality.IncidentResponse incident)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

﻿using STF.Crm.Dynamics.DTO.Quality;
using System.ServiceModel;

namespace STF.Crm.Dynamics.WS
{
    [ServiceContract]
    public interface ICrmDynamicsService
    {

        [OperationContract]
        CustomerResponse GetCustomer(string customerId);

        [OperationContract]
        void SendCustomer(CustomerResponse customer);

        [OperationContract]
        void SendIncident(IncidentResponse incident);
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using STF.Crm.Dynamics.BLL;
using STF.Crm.Dynamics.BLL.Operation;

namespace STF.Crm.Dynamics.UnitTest
{
    [TestClass]
    public class OperationTest
    {
        [TestMethod]
        public void GetContactTest()
        {
            var customer = CoreFacade.GetCustomerById("30412807");

            Assert.IsNotNull(customer);
            Assert.IsTrue(customer.FullName.Length > 0);
        }
    }
}

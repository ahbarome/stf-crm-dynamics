﻿using System.Runtime.Serialization;

namespace STF.Crm.Dynamics.DTO.Quality
{
    [DataContract]
    public class CustomerResponse
    {
        [DataMember]
        public string CountryId { get; set; }

        [DataMember]
        public string CustomerId { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string SurName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string CellPhoneNumber { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string CityId { get; set; }

        [DataMember]
        public string TenCard { get; set; }

        [DataMember]
        public string Category { get; set; }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace STF.Crm.Dynamics.DTO.Quality
{
    [DataContract]
    public class IncidentResponse
    {
        [DataMember]
        public int TicketNumber { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string ChannelId { get; set; }

        [DataMember]
        public string MediumId { get; set; }

        [DataMember]
        public string Format { get; set; }

        [DataMember]
        public DateTime ReceptionDate { get; set; }

        [DataMember]
        public DateTime ClaimDate { get; set; }

        [DataMember]
        public string CustomerId { get; set; }

        [DataMember]
        public string InvoiceNumber { get; set; }

        [DataMember]
        public DateTime PurchaseDate { get; set; }

        [DataMember]
        public decimal ReferencePrice { get; set; }

        [DataMember]
        public string ReferenceCode { get; set; }

        [DataMember]
        public string ReferenceSize { get; set; }

        [DataMember]
        public string ReferenceColor { get; set; }

        [DataMember]
        public string OperativeCenter { get; set; }

        [DataMember]
        public string CarrierId { get; set; }

        [DataMember]
        public string Zone { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string IncidentType { get; set; }

        [DataMember]
        public string CountryId { get; set; }
    }
}
